use std::sync::mpsc;
use std::thread;
use std::time::Duration;

static N: i32 = 2; // number of threads to spawn
static NUM_MSG: i32 = 3;

// You should modify main() to spawn threads and communicate using channels
fn main() {
    let (tx, rx) = mpsc::channel();


    let mut children: Vec<thread::JoinHandle<()>> = vec![];
    // Spawn threads
    for i in 0..N {
        let tx_clone = mpsc::Sender::clone(&tx);
        let temp = i;
        let handle = thread::spawn(move || {
            for _j in 0..NUM_MSG {
                let msg = String::from(format!("Msg from {}",temp));
                tx_clone.send(msg).unwrap();
                thread::sleep(Duration::from_secs(1));
            }
        });
        children.push(handle);
    }
    let mut msg_recvd = 0; 
    for received in rx {
        println!("Got: {}", received);
        msg_recvd += 1;
        if msg_recvd >= NUM_MSG*N {
            break;
        }
    }

    for _i in 0..N {
        let handle = children.remove(0);
        handle.join().expect("Couldn't join");
    }
    println!("Threads finished!");
}
