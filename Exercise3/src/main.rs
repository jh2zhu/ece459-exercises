use std::thread;

static N: i32 = 10;  // number of threads to spawn

// You should modify main() to spawn multiple threads and join() them
fn main() {
    // Vector declaration for holding thread children
    let mut children: Vec<thread::JoinHandle<()>> = vec![];
    // Spawn threads
    for i in 0..N {
        let temp = i;
        let handle = thread::spawn(move || {
            println!("hi number {} from the spawned thread!", temp);
        });
        children.push(handle);
    }
    // Join threads
    for _ii in 0..N {
        let handle = children.remove(0);
        handle.join().expect("Couldn't join on the associated thread");
    }
    println!("Threads finished");
}
