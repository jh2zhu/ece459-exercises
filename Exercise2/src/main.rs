// You should implement the following function
fn fibonacci_number(n: u32) -> u32 {
    let mut num: [u32; 2] = [0,1];
    let mut fib_idx: u32 = 1; // first fib number already calculated
    while fib_idx != n {
       let temp = num[1];
       num[1] += num[0];
       num[0] = temp;
       fib_idx += 1
    }
    
    num[1]
}


fn main() {
    println!("{}", fibonacci_number(10));
}
